var fs = require("fs");
var Eris = require("eris");

var config = JSON.parse(fs.readFileSync("./config.json"));


var bot = new Eris(config.token);

bot.on("ready", () => {
	console.log("Connected.");
});


bot.on("messageCreate", msg => {
	if (!msg.content.startsWith(config.prefix)) return;

	if (msg.content == config.prefix + config.commands.joinVoiceChannel) {
		if (msg.member == null) {
			bot.createMessage(msg.channel.id, "This command only works in guilds.");
			return;
		}
		else if (msg.member.voiceState.channelID == null) {
			bot.createMessage(msg.channel.id, "You must be in a voice channel in this guild.");
			return;
		}
		try {
			bot.joinVoiceChannel(msg.member.voiceState.channelID);
		} catch (e) {
			bot.createMessage(msg.channel.id, "Unable to join voice channel.");
		}
	}

	else if (msg.content == config.prefix + config.commands.listSounds) {
		getAllSounds().then(sounds => {
			var soundString = sounds.map(e => e.split(".")[0]).join(" ");
			bot.createMessage(msg.channel.id, "List of " + sounds.length + " sounds: ```" + soundString + "```");
		});
	}

	else if (msg.content == config.prefix + config.commands.playRandom) {
		if (msg.member == null) {
			bot.createMessage(msg.channel.id, "This command only works in guilds.");
			return;
		}

		var connection = bot.voiceConnections.find(v => v.id == msg.member.guild.id);
		if (connection == null) {
			bot.createMessage(msg.channel.id, "I'm not in a voice channel. Use " + config.prefix + config.commands.joinVoiceChannel);
			return;
		}
		playRandom(connection);
	}

	else if (msg.content.startsWith(config.prefix + config.commands.playSound)) {
		getAllSounds().then(sounds => {
			var splitString = msg.content.split(" ");
			if (splitString.length < 2) {
				bot.createMessage(msg.channel.id, "That's not how this command works");
				return;
			}
			var searchString = splitString[1].trim();
			var sound = sounds.find(e => e.startsWith(searchString));
			if (sound == null) {
				bot.createMessage(msg.channel.id, "Sound not found: " + searchString);
				return;
			}
			var connection = bot.voiceConnections.find(v => v.id == msg.member.guild.id);
			if (connection == null) {
				bot.createMessage(msg.channel.id, "I'm not in a voice channel. Use " + config.prefix + config.commands.joinVoiceChannel);
				return;
			}
			playSound(connection, sound);
		});
	}
});


// Plays a random sound if someone joins a voice channel the bot is in
bot.on("voiceChannelJoin", (member, channel) => {
	var connection = bot.voiceConnections.find(v => v.id == member.guild.id);
	if (connection != null && !member.bot && connection.channelID == channel.id) {
		playRandom(connection);
	}
});

bot.on("voiceChannelSwitch", (member, channel, oldChannel) => {
	var connection = bot.voiceConnections.find(v => v.id == member.guild.id);
	if (connection != null && !member.bot && connection.channelID == channel.id) {
		playRandom(connection);
	}
});


// Returns an array of file names for sounds. Pretty useless, as readdir could be used directly
function getAllSounds() {
	return new Promise((resolve, reject) => {
		fs.readdir(config.soundsDirectory, (err, files) => {
			if (err) {
				console.log("Error reading " + config.soundsDirectory);
				process.exit();
			}
			else {
				resolve(files);
			}
		});
	});
}

function playRandom(voiceConnection) {
	getAllSounds().then(sounds => {
		var randomSound = sounds[Math.floor(Math.random()*sounds.length)];
		playSound(voiceConnection, randomSound);
	});
}

function playSound(voiceConnection, sound) {
	if (voiceConnection.playing) {
		voiceConnection.stopPlaying();
	}
	voiceConnection.play(config.soundsDirectory + "/" + sound);
}


bot.connect();